/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.7.17-log : Database - vertx
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`vertx` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `vertx`;

/*Table structure for table `roles_perms` */

DROP TABLE IF EXISTS `roles_perms`;

CREATE TABLE `roles_perms` (
  `role` varchar(255) NOT NULL,
  `perm` varchar(255) NOT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_category` */

DROP TABLE IF EXISTS `t_category`;

CREATE TABLE `t_category` (
  `id` varchar(100) NOT NULL,
  `c_category_name` varchar(200) DEFAULT NULL,
  `c_category_desc` varchar(1024) DEFAULT NULL,
  `c_active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `c_category_name_UNIQUE` (`c_category_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_charity` */

DROP TABLE IF EXISTS `t_charity`;

CREATE TABLE `t_charity` (
  `id` varchar(100) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_display_img` varchar(500) DEFAULT NULL,
  `c_description` varchar(1024) DEFAULT NULL,
  `c_url` varchar(500) DEFAULT NULL,
  `c_featured` binary(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_charity_categories` */

DROP TABLE IF EXISTS `t_charity_categories`;

CREATE TABLE `t_charity_categories` (
  `c_charity_id` varchar(100) NOT NULL,
  `c_category_id` varchar(100) NOT NULL,
  PRIMARY KEY (`c_charity_id`,`c_category_id`),
  KEY `c_category_id` (`c_category_id`),
  CONSTRAINT `t_charity_categories_ibfk_1` FOREIGN KEY (`c_category_id`) REFERENCES `t_category` (`id`),
  CONSTRAINT `t_charity_categories_ibfk_2` FOREIGN KEY (`c_charity_id`) REFERENCES `t_charity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_charity_images` */

DROP TABLE IF EXISTS `t_charity_images`;

CREATE TABLE `t_charity_images` (
  `c_charity_id` varchar(100) NOT NULL,
  `c_image_url` varchar(500) NOT NULL,
  KEY `c_charity_id` (`c_charity_id`),
  CONSTRAINT `t_charity_images_ibfk_1` FOREIGN KEY (`c_charity_id`) REFERENCES `t_charity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_user_charity` */

DROP TABLE IF EXISTS `t_user_charity`;

CREATE TABLE `t_user_charity` (
  `c_charity_id` varchar(100) NOT NULL,
  `c_user_name` varchar(45) NOT NULL,
  `c_block` bit(1) DEFAULT NULL,
  `c_favourite` bit(1) DEFAULT NULL,
  PRIMARY KEY (`c_charity_id`,`c_user_name`),
  KEY `c_user_name` (`c_user_name`),
  CONSTRAINT `t_user_charity_ibfk_1` FOREIGN KEY (`c_charity_id`) REFERENCES `t_charity` (`id`),
  CONSTRAINT `t_user_charity_ibfk_2` FOREIGN KEY (`c_user_name`) REFERENCES `user` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_salt` varchar(255) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `username` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`username`,`role`),
  KEY `fk_roles` (`role`),
  CONSTRAINT `fk_roles` FOREIGN KEY (`role`) REFERENCES `roles_perms` (`role`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `user` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
