package com.aceandblue.ichange;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        LOGGER.info("Starting MainVerticle ");
        vertx.deployVerticle("com.aceandblue.ichange.DatabaseVerticle", res1 -> {
            LOGGER.info("Starting DatabaseVerticle ");
            if (res1.succeeded()) {
                LOGGER.info("Starting  HttpServerVerticle");
                vertx.deployVerticle("com.aceandblue.ichange.HttpServerVerticle", new DeploymentOptions().setInstances(2), res2 -> {
                    if (res2.succeeded()) {
                        startFuture.complete();
                    } else {
                        startFuture.fail(res2.cause());
                    }
                });
            } else {
                startFuture.fail(res1.cause());
            }
        });
    }
}