package com.aceandblue.ichange;

import com.aceandblue.ichange.config.Config;
import com.aceandblue.ichange.config.QueueConfig;
import com.aceandblue.ichange.service.CharityService;
import com.aceandblue.ichange.service.UserService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.serviceproxy.ProxyHelper;

public class DatabaseVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseVerticle.class);

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        LOGGER.info("Started DatabaseVerticle");
        Config.setJDBCClient(
                JDBCClient.createShared(
                        vertx,
                        new JsonObject()
                                .put("url", config().getString(Config.CONFIG_APPDB_JDBC_URL, "jdbc:mysql://localhost:3306/vertx"))
                                .put("driver_class", config().getString(Config.CONFIG_APPDB_JDBC_DRIVER_CLASS, "com.mysql.jdbc.Driver"))
                                .put("max_pool_size", config().getInteger(Config.CONFIG_APPDB_JDBC_MAX_POOL_SIZE, 30))
                                .put("user", "root")

                )
        );

        Config.getJDBCClient().getConnection(ar -> {
            if (ar.failed()) {
                LOGGER.error("Could not open a database connection", ar.cause());
                startFuture.fail(ar.cause());
            } else {
                LOGGER.info("Database connection working fine.");
                ProxyHelper.registerService(UserService.class, vertx, UserService.create(), QueueConfig.ACCOUNTS_SERIVCE);
                ProxyHelper.registerService(CharityService.class, vertx, CharityService.create(), QueueConfig.CHARITY_SERIVCE);
                startFuture.complete();
            }
        });


    }

}