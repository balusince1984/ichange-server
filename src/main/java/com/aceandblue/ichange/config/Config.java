package com.aceandblue.ichange.config;

import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.jdbc.JDBCClient;

/**
 * Created by avance on 7/30/2017.
 */
public class Config {

    public static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";
    public static final String CONFIG_APPDB_JDBC_URL = "appdb.jdbc.url";
    public static final String CONFIG_APPDB_JDBC_DRIVER_CLASS = "appdb.jdbc.driver_class";
    public static final String CONFIG_APPDB_JDBC_MAX_POOL_SIZE = "appdb.jdbc.max_pool_size";

    private static JDBCClient dbClient;

    private static AuthProvider authProvider;

    private static JWTAuth jwtAuth;

    public static void setJDBCClient(JDBCClient dbClient) {
        Config.dbClient = dbClient;
    }

    public static JDBCClient getJDBCClient() {
        return Config.dbClient;
    }

    public static void setAuthProvider(AuthProvider authProvider) {
        Config.authProvider = authProvider;
    }

    public static AuthProvider getAuthProvider() {
        return authProvider;
    }

    public static JWTAuth getJwtAuth() {
        return jwtAuth;
    }

    public static void setJwtAuth(JWTAuth jwtAuth) {
        Config.jwtAuth = jwtAuth;
    }
}
