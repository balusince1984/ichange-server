package com.aceandblue.ichange;

import com.aceandblue.ichange.api.CharityAPI;
import com.aceandblue.ichange.api.UserAPI;
import com.aceandblue.ichange.config.Config;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.jdbc.JDBCAuth;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.dropwizard.MetricsService;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

/**
 * Created by balakrishnav on 7/30/2017.
 */
public class HttpServerVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServerVerticle.class);

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        /*new HttpServerOptions()
                .setSsl(true)
                .setKeyStoreOptions(new JksOptions()
                        .setPath("server-keystore.jks")
                        .setPassword("secret"))*/
        HttpServer server = vertx.createHttpServer();
        Config.setAuthProvider(JDBCAuth.create(vertx, Config.getJDBCClient()));
        Config.setJwtAuth(
                JWTAuth.create(vertx, new JsonObject()
                        .put("keyStore", new JsonObject()
                                .put("path", "keystore.jceks")
                                .put("type", "jceks")
                                .put("password", "secret")))
        );
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/api/*").handler(JWTAuthHandler.create(Config.getJwtAuth()));
        MetricsService service = MetricsService.create(vertx);
        BridgeOptions options = new BridgeOptions().
                addOutboundPermitted(
                        new PermittedOptions().
                                setAddress("metrics")
                );
        router.route("/eventbus/*").handler(SockJSHandler.create(vertx).bridge(options));
        vertx.setPeriodic(1000, t -> {
            JsonObject metrics = service.getMetricsSnapshot(vertx.eventBus());
            vertx.eventBus().publish("metrics", metrics);
        });

        CharityAPI.init(vertx, router);
        UserAPI.init(vertx, router);

        int portNumber = config().getInteger(Config.CONFIG_HTTP_SERVER_PORT, 8080);
        server
                .requestHandler(router::accept)
                .listen(portNumber, ar -> {
                    if (ar.succeeded()) {
                        LOGGER.info("HTTP server running on port " + portNumber);
                        startFuture.complete();
                    } else {
                        LOGGER.error("Could not start a HTTP server", ar.cause());
                        startFuture.fail(ar.cause());
                    }
                });
    }


}
