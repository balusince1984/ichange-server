package com.aceandblue.ichange.service.impl;

import com.aceandblue.ichange.config.Config;
import com.aceandblue.ichange.service.UserService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.jdbc.JDBCAuth;

import static com.aceandblue.ichange.service.impl.DBQueryService.query;

/**
 * Created by avance on 7/30/2017.
 */
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private String getUserByEmail = "select firstName,lastName,username,password from user where username=?";

    private String createUser = "insert into user(firstName, lastName, username, password, password_salt) values(?,?,?,?,?)";

    public UserServiceImpl() {
    }

    @Override
    public UserService registerUser(String firstName, String lastName, String email, String password, Handler<AsyncResult<JsonObject>> resultHandler) {
        query(getUserByEmail, new JsonArray().add(email), res -> {
            if (res.succeeded()) {
                if (res.result().getResults().size() > 0) {
                    resultHandler.handle(Future.failedFuture("User with email already exists"));
                } else {
                    String salt = ((JDBCAuth) Config.getAuthProvider()).generateSalt();
                    String hash = ((JDBCAuth) Config.getAuthProvider()).computeHash(password, salt);
                    query(createUser, new JsonArray().add(firstName).add(lastName).add(email).add(hash).add(salt), res1 -> {
                        if (res1.succeeded()) {
                            resultHandler.handle(Future.succeededFuture());
                        }else{
                            resultHandler.handle(Future.failedFuture(res.cause()));
                        }
                    });
                }
            }else{
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }
}
