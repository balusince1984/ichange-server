package com.aceandblue.ichange.service.impl;

import com.aceandblue.ichange.dto.CategoryDTO;
import com.aceandblue.ichange.dto.CharityDTO;
import com.aceandblue.ichange.service.CharityService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import static com.aceandblue.ichange.service.impl.DBQueryService.*;

/**
 * Created by balakrishnav on 8/3/2017.
 */
public class CharityServiceImpl implements CharityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final static String columns = "id,c_name,c_display_img,c_description,c_url,c_featured,c_block,c_favourite";

    private final static String getActiveCategories = "SELECT id,c_category_name,c_category_desc,c_active FROM t_category where c_active = 1";

    private final static String getFeaturedCharities = "SELECT " + columns + " FROM t_charity tch " +
            "LEFT JOIN t_user_charity tuc ON (tuc.c_charity_id = tch.id AND tuc.c_user_name = ?) WHERE c_featured = 1";

    private final static String getCharitiesInCategory = "SELECT " + columns + " FROM t_charity tch " +
            "JOIN t_charity_categories tcat ON (tch.id=tcat.c_charity_id AND tcat.c_category_id = ?) " +
            "LEFT JOIN t_user_charity tuc ON (tuc.c_charity_id = tch.id AND tuc.c_user_name = ?)";

    private final static String getUserCharities = "SELECT " + columns + " FROM t_charity tch " +
            "JOIN t_user_charity tuc ON (tuc.c_charity_id = tch.id AND c_user_name = ?) WHERE c_featured = 1";

    private final static String blockCharity = "INSERT INTO t_user_charity(c_charity_id,c_user_name,c_block,c_favourite) VALUES(?,?,1,0)  " +
            "ON DUPLICATE KEY UPDATE c_block = 1, c_favourite = 0";

    private final static String setFavourite = "INSERT INTO t_user_charity(c_user_name, c_charity_id,c_block,c_favourite) VALUES(?,?,0,1)  " +
            "ON DUPLICATE KEY UPDATE c_block = 0, c_favourite = 1";

    public CharityServiceImpl() {
    }


    @Override
    public CharityService getActiveCategories(Handler<AsyncResult<JsonObject>> resultHandler) {
        query(getActiveCategories, res -> {
            if (res.succeeded()) {
                JsonArray jsonArray = new JsonArray();
                res.result().getResults().stream().map(json -> {
                    CategoryDTO categoryDTO = new CategoryDTO();
                    categoryDTO.setId(json.getString(0));
                    categoryDTO.setCategoryName(json.getString(1));
                    categoryDTO.setCategoryDescription(json.getString(2));
                    return JsonObject.mapFrom(categoryDTO);
                }).forEach(jsonArray::add);
                resultHandler.handle(Future.succeededFuture(buildResponseObject(jsonArray)));
            } else {
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    @Override
    public CharityService getFeaturedCharities(String user, Handler<AsyncResult<JsonObject>> resultHandler) {
        query(getFeaturedCharities, new JsonArray().add(user), res -> {
            if (res.succeeded()) {
                JsonArray jsonArray = new JsonArray();
                res.result().getResults().stream().map(json -> {
                    return buildCharityDTO(json);
                }).forEach(jsonArray::add);
                resultHandler.handle(Future.succeededFuture(buildResponseObject(jsonArray)));
            } else {
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    @Override
    public CharityService getCharitiesInCategory(String user, String category, Handler<AsyncResult<JsonObject>> resultHandler) {
        query(getCharitiesInCategory, new JsonArray().add(category).add(user), res -> {
            if (res.succeeded()) {
                JsonArray jsonArray = new JsonArray();
                res.result().getResults().stream().map(json -> {
                    return buildCharityDTO(json);
                }).forEach(jsonArray::add);
                resultHandler.handle(Future.succeededFuture(buildResponseObject(jsonArray)));
            } else {
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    @Override
    public CharityService getUserCharities(String user, Handler<AsyncResult<JsonObject>> resultHandler) {
        query(getUserCharities, new JsonArray().add(user), res -> {
            if (res.succeeded()) {
                JsonArray jsonArray = new JsonArray();
                res.result().getResults().stream().map(json -> {
                    return buildCharityDTO(json);
                }).forEach(jsonArray::add);
                resultHandler.handle(Future.succeededFuture(buildResponseObject(jsonArray)));
            } else {
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    @Override
    public CharityService blockCharity(String user, String charityId, Handler<AsyncResult<JsonObject>> resultHandler) {
        update(blockCharity, new JsonArray().add(user).add(charityId), res -> {
            if (res.succeeded()) {
                resultHandler.handle(Future.succeededFuture(buildResponseObject()));
            } else {
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    @Override
    public CharityService setFavourite(String user, String charityId, Handler<AsyncResult<JsonObject>> resultHandler) {
        update(setFavourite, new JsonArray().add(user).add(charityId), res -> {
            if (res.succeeded()) {
                resultHandler.handle(Future.succeededFuture(buildResponseObject()));
            } else {
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    private JsonObject buildCharityDTO(JsonArray json) {
        //id,c_name,c_display_img,c_description,c_url,c_featured,c_block,c_favourite
        int index = 0;
        CharityDTO charityDTO = new CharityDTO();
        charityDTO.setId(json.getString(index++));
        charityDTO.setName(json.getString(index++));
        charityDTO.setDisplayImage(json.getString(index++));
        charityDTO.setDescription(json.getString(index++));
        charityDTO.setUrl(json.getString(index++));
        charityDTO.setFeatured(json.getBoolean(index++));
        charityDTO.setBlock(json.getBoolean(index++));
        charityDTO.setFavourite(json.getBoolean(index++));
        return JsonObject.mapFrom(charityDTO);
    }
}
