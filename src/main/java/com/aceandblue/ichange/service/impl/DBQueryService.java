package com.aceandblue.ichange.service.impl;

import com.aceandblue.ichange.config.Config;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;

/**
 * Created by balakrishnav on 8/5/2017.
 */
public class DBQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBQueryService.class);

    public static JsonObject buildResponseObject(JsonArray jsonArray) {
        return new JsonObject().put("results", jsonArray).put("success", true);
    }

    public static JsonObject buildResponseObject() {
        return new JsonObject().put("success", true);
    }

    public static void query(String query, Handler<AsyncResult<ResultSet>> resultSetHandler) {
        LOGGER.info("Executing query {0} ", query);
        Config.getJDBCClient().getConnection(car -> {
            if (car.succeeded()) {
                SQLConnection connection = car.result();
                try {
                    connection.query(query, res -> {
                        if (res.succeeded()) {
                            resultSetHandler.handle(res);
                        } else {
                            LOGGER.fatal("Query Execution Failed : ", res.cause());
                            resultSetHandler.handle(Future.failedFuture("Query Execution Failed"));
                        }
                        connection.close();
                    });
                } catch (Throwable t) {
                    resultSetHandler.handle(Future.failedFuture(t));
                }
            } else {
                LOGGER.fatal("Unable to connect to database: ", car.cause());
                resultSetHandler.handle(Future.failedFuture("Unable to connect to the database"));
            }
        });
    }

    public static void query(String query, JsonArray jsonArray, Handler<AsyncResult<ResultSet>> resultSetHandler) {
        LOGGER.info("Executing query {0} with params {1} ", query, jsonArray);
        Config.getJDBCClient().getConnection(car -> {
            if (car.succeeded()) {
                SQLConnection connection = car.result();
                connection.queryWithParams(query, jsonArray, res -> {
                    if (res.succeeded()) {
                        resultSetHandler.handle(res);
                    } else {
                        LOGGER.fatal("Query Execution Failed : ", res.cause());
                        resultSetHandler.handle(Future.failedFuture("Query Execution Failed"));

                    }
                    connection.close();
                });
            } else {
                LOGGER.fatal("Unable to connect to database: ", car.cause());
                resultSetHandler.handle(Future.failedFuture("Unable to connect to the database"));
            }
        });
    }

    public static void update(String query, JsonArray jsonArray, Handler<AsyncResult<ResultSet>> resultSetHandler) {
        LOGGER.info("Executing update query {0} with params {1} ", query, jsonArray);
        Config.getJDBCClient().getConnection(car -> {
            if (car.succeeded()) {
                SQLConnection connection = car.result();
                connection.updateWithParams(query, jsonArray, res -> {
                    if (res.succeeded()) {
                        resultSetHandler.handle(Future.succeededFuture());
                    } else {
                        LOGGER.fatal("Query Execution Failed : ", res.cause());
                        resultSetHandler.handle(Future.failedFuture("Query Execution Failed"));
                    }
                    connection.close();
                });
            } else {
                LOGGER.fatal("Unable to connect to database: ", car.cause());
                resultSetHandler.handle(Future.failedFuture("Unable to connect to the database"));
            }
        });
    }

}
