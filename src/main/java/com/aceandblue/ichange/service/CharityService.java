package com.aceandblue.ichange.service;

import com.aceandblue.ichange.service.impl.CharityServiceImpl;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

/**
 * Created by avance on 8/3/2017.
 */
@ProxyGen
public interface CharityService {

    static CharityService create() {
        return new CharityServiceImpl();
    }

    static CharityService createProxy(Vertx vertx, String address) {
        return new CharityServiceVertxEBProxy(vertx, address);
    }

    @Fluent
    CharityService getActiveCategories(Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    CharityService getFeaturedCharities(String user, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    CharityService getCharitiesInCategory(String user,String category, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    CharityService getUserCharities(String user, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    CharityService blockCharity(String user, String charityId, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    CharityService setFavourite(String user, String charityId, Handler<AsyncResult<JsonObject>> resultHandler);
}
