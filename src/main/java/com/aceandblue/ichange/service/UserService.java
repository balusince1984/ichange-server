package com.aceandblue.ichange.service;

import com.aceandblue.ichange.service.impl.UserServiceImpl;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

/**
 * Created by avance on 7/30/2017.
 */
@ProxyGen
public interface UserService {

    static UserService create() {
        return new UserServiceImpl();
    }

    static UserService createProxy(Vertx vertx, String address) {
        return new UserServiceVertxEBProxy(vertx, address);
    }

    @Fluent
    UserService registerUser(String firstName, String lastName, String email, String password, Handler<AsyncResult<JsonObject>> resultHandler);

}
