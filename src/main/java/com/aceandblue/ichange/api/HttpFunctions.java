package com.aceandblue.ichange.api;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

/**
 * Created by balakrishnav on 8/5/2017.
 */
public class HttpFunctions {

    public static void buildHTTP500(RoutingContext context, Throwable t) {
        context.response().setStatusCode(500);
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(new JsonObject()
                .put("success", false)
                .put("error", t.getMessage()).encode());
    }

    public static void buildHTTP200(RoutingContext context, JsonObject jsonObject) {
        context.response().setStatusCode(200);
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(jsonObject.encode());
    }

    public static void buildHTTP201(RoutingContext context) {
        context.response().setStatusCode(201);
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(new JsonObject().put("success", true).encode());
    }

    public static void buildHTTP400(RoutingContext context, JsonObject t) {
        context.response().setStatusCode(500);
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(t.encode());
    }
}
