package com.aceandblue.ichange.api;

import com.aceandblue.ichange.config.QueueConfig;
import com.aceandblue.ichange.service.CharityService;

import static com.aceandblue.ichange.api.HttpFunctions.*;

import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

/**
 * Created by balakrishnav on 8/3/2017.
 */
public class CharityAPI {

    private static final Logger LOGGER = LoggerFactory.getLogger(CharityAPI.class);

    private static CharityService charityService;


    public static void init(Vertx vertx, Router router) {
        charityService = CharityService.createProxy(vertx, QueueConfig.CHARITY_SERIVCE);
        router.get("/api/charity/categories").handler(CharityAPI::getActiveCategories);
        router.get("/api/charity/featured").handler(CharityAPI::getFeaturedCharities);
        router.get("/api/charity/favorites").handler(CharityAPI::getUserCharities);
        router.get("/api/charity/list/:category").handler(CharityAPI::getCharitiesInCategory);
        router.put("/api/charity/favorite/:id").handler(CharityAPI::setFavourite);
        router.put("/api/charity/block/:id").handler(CharityAPI::blockCharity);

    }

    public static void getActiveCategories(RoutingContext context) {
        charityService.getActiveCategories(res -> {
            if (res.succeeded()) {
                buildHTTP200(context, res.result());
            } else {
                buildHTTP500(context, res.cause());
            }
        });
    }

    public static void getFeaturedCharities(RoutingContext context) {
        charityService.getFeaturedCharities(context.user().principal().getString("username"), res -> {
            if (res.succeeded()) {
                buildHTTP200(context, res.result());
            } else {
                buildHTTP500(context, res.cause());
            }
        });
    }

    public static void getCharitiesInCategory(RoutingContext context) {
        charityService.getCharitiesInCategory(context.user().principal().getString("username"), context.pathParam("category"), res -> {
            if (res.succeeded()) {
                buildHTTP200(context, res.result());
            } else {
                buildHTTP500(context, res.cause());
            }
        });
    }

    public static void getUserCharities(RoutingContext context) {
        charityService.getUserCharities(context.user().principal().getString("username"), res -> {
            if (res.succeeded()) {
                buildHTTP200(context, res.result());
            } else {
                buildHTTP500(context, res.cause());
            }
        });
    }

    public static void blockCharity(RoutingContext context) {
        charityService.blockCharity(context.user().principal().getString("username"), context.pathParam("id"), res -> {
            if (res.succeeded()) {
                buildHTTP201(context);
            } else {
                buildHTTP500(context, res.cause());
            }
        });
    }

    public static void setFavourite(RoutingContext context) {
        charityService.setFavourite(context.user().principal().getString("username"), context.pathParam("id"), res -> {
            if (res.succeeded()) {
                buildHTTP201(context);
            } else {
                buildHTTP500(context, res.cause());
            }
        });
    }

}

