package com.aceandblue.ichange.api;

import com.aceandblue.ichange.config.Config;
import com.aceandblue.ichange.config.QueueConfig;
import com.aceandblue.ichange.service.UserService;
import static com.aceandblue.ichange.api.HttpFunctions.*;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.Arrays;

/**
 * Created by balakrishnav on 8/3/2017.
 */
public class UserAPI {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAPI.class);

    private static UserService userService;

    public static void init(Vertx vertx, Router router) {
        userService = UserService.createProxy(vertx, QueueConfig.ACCOUNTS_SERIVCE);
        router.get("/login").handler(UserAPI::logIn);
        router.get("/logout").handler(UserAPI::logOut);
        router.post("/register").handler(UserAPI::registerUser);
        router.post("/resetPassword/:email").handler(UserAPI::resetPassword);
    }


    private static void registerUser(RoutingContext context) {
        JsonObject user = context.getBodyAsJson();
        if (!validateJsonDocument(context, user, "firstName", "lastName", "email", "password")) {
            return;
        }
        LOGGER.info("Registering user. " + user.toString());
        String password = user.getString("password");
        userService.registerUser(user.getString("firstName"), user.getString("lastName"), user.getString("email"), user.getString("password"), reply -> {
            if (reply.succeeded()) {
                buildHTTP200(context, reply.result());
            } else {
                buildHTTP500(context, reply.cause());
            }
        });
    }

    private static void logIn(RoutingContext context) {
        JsonObject credentials = new JsonObject()
                .put("username", context.request().getHeader("login"))
                .put("password", context.request().getHeader("password"));
        Config.getAuthProvider().authenticate(credentials, authResult -> {
            if (authResult.succeeded()) {
                User user = authResult.result();
                String token = Config.getJwtAuth().generateToken(
                        new JsonObject().put("username", context.request().getHeader("login")),
                        new JWTOptions().setSubject("App").setIssuer("Vert.x"));
                context.response().putHeader("Content-Type", "text/plain").end(token);
            } else {
                context.fail(401);
            }
        });
    }

    private static void logOut(RoutingContext context) {
        context.clearUser();  // <2>
        context.response()
                .setStatusCode(302)
                .putHeader("Location", "/")
                .end();
    }

    private static void resetPassword(RoutingContext context) {

    }

    private static boolean validateJsonDocument(RoutingContext context, JsonObject user, String... expectedKeys) {
        if (!Arrays.stream(expectedKeys).allMatch(user::containsKey)) {
            LOGGER.error("Bad user creation JSON payload: " + user.encodePrettily() + " from " + context.request().remoteAddress());
            buildHTTP400(context,new JsonObject()
                    .put("success", false)
                    .put("error", "Bad request payload"));
            return false;
        }
        return true;
    }
}
